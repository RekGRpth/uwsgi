FROM rekgrpth/pdf
ENV GROUP=uwsgi \
    PYTHONIOENCODING=UTF-8 \
    USER=uwsgi
RUN set -ex \
    && apk update --no-cache \
    && apk upgrade --no-cache \
    && addgroup -S "${GROUP}" \
    && adduser -D -S -h "${HOME}" -s /sbin/nologin -G "${GROUP}" "${USER}" \
    && apk add --no-cache --virtual .uwsgi-rundeps \
        ipython \
        uwsgi \
        uwsgi-python3 \
    && ln -s pip3 /usr/bin/pip \
    && ln -s pydoc3 /usr/bin/pydoc \
    && ln -s python3 /usr/bin/python \
    && ln -s python3-config /usr/bin/python-config
